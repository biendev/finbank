<?php
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version		1.0.0
 * 
 * Blog Page Default Video Post Format Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_post_metadata = !is_home() ? explode(',', $cmsmasters_metadata) : array();


$date = (in_array('date', $cmsmasters_post_metadata) || is_home()) ? true : false;
$categories = (get_the_category() && (in_array('categories', $cmsmasters_post_metadata) || is_home())) ? true : false;
$author = (in_array('author', $cmsmasters_post_metadata) || is_home()) ? true : false;
$comments = (comments_open() && (in_array('comments', $cmsmasters_post_metadata) || is_home())) ? true : false;
$likes = (in_array('likes', $cmsmasters_post_metadata) || is_home()) ? true : false;
$more = (in_array('more', $cmsmasters_post_metadata) || is_home()) ? true : false;


$cmsmasters_post_video_type = get_post_meta(get_the_ID(), 'cmsmasters_post_video_type', true);

$cmsmasters_post_video_link = get_post_meta(get_the_ID(), 'cmsmasters_post_video_link', true);

$cmsmasters_post_video_links = get_post_meta(get_the_ID(), 'cmsmasters_post_video_links', true);

?>

<!--_________________________ Start Video Article _________________________ -->

<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_post_default'); ?>>
	<?php 
	if ($date || $likes || $comments) {
		echo '<div class="cmsmasters_post_info entry-meta">';
		
			if (is_sticky()) {
				echo '<div class="cmsmasters_sticky cmsmasters_theme_icon_sticky"></div>';
			}
			
			$date ? payday_loans_get_post_date('page', 'default') : '';
			
			if ($likes || $comments) {
				echo '<div class="cmsmasters_post_info_bot">';
				
					$likes ? payday_loans_get_post_likes('page') : '';
			
					$comments ? payday_loans_get_post_comments('page') : '';
					
				echo '</div>';
			}
			
		echo '</div>';
	}
	?>
	<div class="cmsmasters_post_cont">
		<?php		
		
		if (!post_password_required()) {
			if ($cmsmasters_post_video_type == 'selfhosted' && !empty($cmsmasters_post_video_links) && sizeof($cmsmasters_post_video_links) > 0 || $cmsmasters_post_video_type == 'embedded' && $cmsmasters_post_video_link != '' || has_post_thumbnail()) {
				echo '<div class="cmsmasters_post_cont_img_wrap">';
				
					if ($cmsmasters_post_video_type == 'selfhosted' && !empty($cmsmasters_post_video_links) && sizeof($cmsmasters_post_video_links) > 0) {
						$video_size = cmsmasters_image_thumbnail_list();
						
						
						$attrs = array( 
							'preload'  => 'none', 
							'height'   => $video_size['post-thumbnail']['height'], 
							'width'    => $video_size['post-thumbnail']['width'] 
						);
						
						
						if (has_post_thumbnail()) {
							$video_poster = wp_get_attachment_image_src((int) get_post_thumbnail_id(get_the_ID()), 'post-thumbnail');
							
							
							$attrs['poster'] = $video_poster[0];
						}
						
						
						foreach ($cmsmasters_post_video_links as $cmsmasters_post_video_link_url) {
							$attrs[substr(strrchr($cmsmasters_post_video_link_url, '.'), 1)] = $cmsmasters_post_video_link_url;
						}
						
						
						echo '<div class="cmsmasters_video_wrap">' . 
							wp_video_shortcode($attrs) . 
						'</div>';
					} elseif ($cmsmasters_post_video_type == 'embedded' && $cmsmasters_post_video_link != '') {
						global $wp_embed;
						
						
						$video_size = cmsmasters_image_thumbnail_list();
						
						
						echo '<div class="cmsmasters_video_wrap">' . 
							do_shortcode($wp_embed->run_shortcode('[embed width="' . $video_size['post-thumbnail']['width'] . '" height="' . $video_size['post-thumbnail']['height'] . '"]' . $cmsmasters_post_video_link . '[/embed]')) . 
						'</div>';
					} elseif (has_post_thumbnail()) {
						payday_loans_thumb(get_the_ID(), 'post-thumbnail', true, false, true, false, true, true, false);
					}
					
				echo '</div>';
			}
		}
		
		echo '<div class="cmsmasters_post_cont_inner_wrap">';
		
			payday_loans_post_heading(get_the_ID(), 'h2');
			
			
			if ($author || $categories) {
				echo '<div class="cmsmasters_post_cont_info entry-meta">';
					
					$author ? payday_loans_get_post_author('page') : '';
					
					$categories ? payday_loans_get_post_category(get_the_ID(), 'category', 'page') : '';
					
				echo '</div>';
			}
			
			
			payday_loans_post_exc_cont();
			
			
			if ($more) {
				echo '<footer class="cmsmasters_post_footer entry-meta">';
					
					$more ? payday_loans_post_more(get_the_ID()) : '';
					
				echo '</footer>';
			}
			
		echo '</div>';
		?>
	</div>
</article>
<!--_________________________ Finish Video Article _________________________ -->

