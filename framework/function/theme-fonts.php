<?php
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version 	1.0.8
 * 
 * Theme Fonts Rules
 * Created by CMSMasters
 * 
 */


function payday_loans_theme_fonts() {
	$cmsmasters_option = payday_loans_get_global_options();
	
	
	$custom_css = "/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version 	1.0.8
 * 
 * Theme Fonts Rules
 * Created by CMSMasters
 * 
 */


/***************** Start Theme Font Styles ******************/

	/* Start Content Font */
	body,
	.widget_tag_cloud a, 
	.header_top .meta_wrap, 
	.widget_calendar tbody td, 
	.widget_custom_contact_info_entries a, 
	.cmsmasters_tabs .cmsmasters_tabs_list_item a, 
	.header_top .meta_wrap a, 
	.cmsmasters_toggles .cmsmasters_toggle_title a, 
	.cmsmasters_items_sort_block .button, 
	.cmsmasters_items_filter_list li a, 
	.cmsmasters_twitter_wrap .cmsmasters_twitter_item_content, 
	.cmsmasters_open_post .cmsmasters_post_tags > a, 
	ul.navigation li a .nav_subtitle {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_content_font_google_font']) . $cmsmasters_option['payday-loans' . '_content_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_content_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_content_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_content_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_content_font_font_style'] . ";
	}
	
	.widget {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_font_size'] - 1) . "px;
	}
	
	.widget_calendar tbody td {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_line_height'] - 3) . "px;
	} 
	
	.cmsmasters_twitter_wrap .cmsmasters_twitter_item_content, 
	.cmsmasters_twitter_wrap .cmsmasters_twitter_item_content a {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_font_size'] + 2) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_line_height'] + 8) . "px;
	}
	
	.cmsmasters_toggles .cmsmasters_toggle_title a, 
	.cmsmasters_tabs .cmsmasters_tabs_list_item a {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_font_size'] + 4) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_line_height'] + 4) . "px;
	}
	
	ul.navigation li a .nav_subtitle {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_line_height'] - 4) . "px;
		text-transform:none;
	}
	
	.header_top .meta_wrap,
	.header_top .meta_wrap a {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_content_font_font_size'] - 2) . "px;
	}
	
	/* Finish Content Font */


	/* Start Link Font */
	a {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_link_font_google_font']) . $cmsmasters_option['payday-loans' . '_link_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_link_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_link_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_link_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_link_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_link_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['payday-loans' . '_link_font_text_decoration'] . ";
	}
	
	a:hover {
		text-decoration:" . $cmsmasters_option['payday-loans' . '_link_hover_decoration'] . ";
	}
	/* Finish Link Font */


	/* Start Navigation Title Font */
	.navigation > li > a, 
	.top_line_nav > li > a, 
	.footer_nav > li > a, 
	.footer_copyright, 
	.navigation .menu-item-mega-container > ul > li > a .nav_title {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_nav_title_font_google_font']) . $cmsmasters_option['payday-loans' . '_nav_title_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_nav_title_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_nav_title_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_nav_title_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_nav_title_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_nav_title_font_text_transform'] . ";
	}
	
	@media only screen and (max-width: 1024px) {
		.navigation > li > a {
			font-size:" . ((int) $cmsmasters_option['payday-loans' . '_nav_title_font_font_size'] - 2) . "px;
		}
	}
	
	.footer_nav > li > a, 
	.footer_copyright {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_nav_title_font_font_size'] - 2) . "px;
	}
	
	.footer_copyright {
		text-transform:none;
	}
	
	.top_line_nav > li > a {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_nav_title_font_font_size'] - 2) . "px;
		text-transform:none;
	}
	
	.navigation .menu-item-mega-container > ul > li > a .nav_title {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_nav_title_font_font_size'] - 2) . "px;
	}
	
	/* Finish Navigation Title Font */


	/* Start Navigation Dropdown Font */
	.navigation ul li a,
	.top_line_nav ul li a {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_nav_dropdown_font_google_font']) . $cmsmasters_option['payday-loans' . '_nav_dropdown_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_nav_dropdown_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_nav_dropdown_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_nav_dropdown_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_nav_dropdown_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_nav_dropdown_font_text_transform'] . ";
	}
	
	.top_line_nav ul li a {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_nav_dropdown_font_font_size'] - 2) . "px;
	}
	
	/* Finish Navigation Dropdown Font */


	/* Start H1 Font */
	h1,
	h1 a,
	.cmsmasters_price_wrap .cmsmasters_currency, 
	.cmsmasters_price_wrap .cmsmasters_price, 
	.cmsmasters_price_wrap .cmsmasters_coins, 
	.logo .title {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_h1_font_google_font']) . $cmsmasters_option['payday-loans' . '_h1_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_h1_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_h1_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_h1_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['payday-loans' . '_h1_font_text_decoration'] . ";
	}
	
	.cmsmasters_dropcap {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_h1_font_google_font']) . $cmsmasters_option['payday-loans' . '_h1_font_system_font'] . ";
		font-weight:" . $cmsmasters_option['payday-loans' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_h1_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['payday-loans' . '_h1_font_text_decoration'] . ";
	}
	
	.cmsmasters_icon_list_items.cmsmasters_icon_list_icon_type_number .cmsmasters_icon_list_item .cmsmasters_icon_list_icon:before,
	.cmsmasters_icon_box.box_icon_type_number:before,
	.cmsmasters_icon_box.cmsmasters_icon_heading_left.box_icon_type_number .icon_box_heading:before {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_h1_font_google_font']) . $cmsmasters_option['payday-loans' . '_h1_font_system_font'] . ";
		font-weight:" . $cmsmasters_option['payday-loans' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_h1_font_font_style'] . ";
	}
	
	.cmsmasters_dropcap.type1 {
		font-size:36px; /* static */
	}
	
	.cmsmasters_dropcap.type2 {
		font-size:20px; /* static */
	}
	
	.headline_outer .headline_inner .headline_icon:before {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_h1_font_font_size'] + 5) . "px;
	}
	
	.headline_outer .headline_inner.align_center .headline_icon:before {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_h1_font_line_height'] + 15) . "px;
	}
	
	.headline_outer .headline_inner.align_left .headline_icon {
		padding-left:" . ((int) $cmsmasters_option['payday-loans' . '_h1_font_font_size'] + 5) . "px;
	}
	
	.headline_outer .headline_inner.align_right .headline_icon {
		padding-right:" . ((int) $cmsmasters_option['payday-loans' . '_h1_font_font_size'] + 5) . "px;
	}
	
	.headline_outer .headline_inner.align_center .headline_icon {
		padding-top:" . ((int) $cmsmasters_option['payday-loans' . '_h1_font_line_height'] + 15) . "px;
	}

	.cmsmasters_price_wrap .cmsmasters_currency, 
	.cmsmasters_price_wrap .cmsmasters_price, 
	.cmsmasters_price_wrap .cmsmasters_coins {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_h1_font_font_size'] + 24 ) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_h1_font_line_height'] + 24 ) . "px;
	}
	
	/* Finish H1 Font */


	/* Start H2 Font */
	h2,
	h2 a,
	.cmsmasters_sitemap_wrap > h1,
	.cmsmasters_sitemap_wrap > h1 a,
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > a {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_h2_font_google_font']) . $cmsmasters_option['payday-loans' . '_h2_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_h2_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_h2_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_h2_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['payday-loans' . '_h2_font_text_decoration'] . ";
	}
	
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > a {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_h2_font_font_size'] - 4) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_h2_font_line_height'] - 4) . "px;
	}
	
	/* Finish H2 Font */


	/* Start H3 Font */
	h3,
	h3 a, 
	.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat_counter_wrap, 
	.cmsmasters_post_date {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_h3_font_google_font']) . $cmsmasters_option['payday-loans' . '_h3_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_h3_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_h3_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_h3_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['payday-loans' . '_h3_font_text_decoration'] . ";
	}
	
	.cmsmasters_post_date {
		font-size:24px;
		line-height:30px;
	}
	
	/* Finish H3 Font */


	/* Start H4 Font */
	h4, 
	h4 a,
	.cmsmasters_stats .cmsmasters_stat_wrap .cmsmasters_stat_title, 
	#fbuilder .one_column label, 
	.wpcf7-list-item-label,
	body .cmsmasters-form-builder .check_parent input[type=checkbox] + label, 
	body .cmsmasters-form-builder .check_parent input[type=radio] + label, 
	.cmsmasters_single_slider .cmsmasters_post_date,
	.cmsmasters_post_masonry .cmsmasters_post_date, 
	.cmsmasters_posts_slider .cmsmasters_slider_post_date {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_h4_font_google_font']) . $cmsmasters_option['payday-loans' . '_h4_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_h4_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_h4_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_h4_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_h4_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['payday-loans' . '_h4_font_text_decoration'] . ";
	}
	
	#fbuilder .one_column label, 
	.wpcf7-list-item-label,
	body .cmsmasters-form-builder .check_parent input[type=checkbox] + label, 
	body .cmsmasters-form-builder .check_parent input[type=radio] + label {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_h4_font_font_size'] - 4) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_h4_font_line_height'] - 4)  . "px;
	}
	
	.cmsmasters_single_slider .cmsmasters_post_date,
	.cmsmasters_posts_slider .cmsmasters_slider_post_date,  
	.cmsmasters_post_masonry .cmsmasters_post_date {
		font-size:18px;
		line-height:20px;
	}
	/* Finish H4 Font */


	/* Start H5 Font */
	h5,
	h5 a, 
	.widgettitle, 
	.widgettitle a, 
	#fbuilder .fform > span, 
	#fbuilder .section_breaks label,
	.page #fbuilder .codepeoplecalculatedfield, 
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li.menu-item-has-children > a, 
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_title, 
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap, 
	.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat_title, 
	.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat_title, 
	.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap, 
	.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat_counter_wrap {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_h5_font_google_font']) . $cmsmasters_option['payday-loans' . '_h5_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_h5_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_h5_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_h5_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['payday-loans' . '_h5_font_text_decoration'] . ";
	}
	
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_h5_font_font_size'] + 32 ) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_h5_font_line_height'] + 32 ) . "px;
	}
	
	.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat_counter_wrap {
		font-size:16px;
		line-height:20px;
	}
	
	.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_h5_font_font_size'] + 32 ) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_h5_font_line_height'] + 46 ) . "px;
	}
	
	.page #fbuilder .codepeoplecalculatedfield {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_h5_font_font_size'] - 2 ) . "px;
	}
	
	/* Finish H5 Font */


	/* Start H6 Font */
	h6,
	h6 a,
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap_archive li,
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap_archive li a,
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category > li > a, 
	.cmsmasters_archive_type .cmsmasters_archive_item_type, 
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li a, 
	.widget_rss ul li .rsswidget, 
	.cmsmasters_table_row_header th,
	.cmsmasters_table_row_footer td,
	.cmsmasters_open_project .project_details_item_title, 
	.cmsmasters_open_project .project_features_item_title, 
	.cmsmasters_open_profile .profile_details_item_title, 
	.cmsmasters_open_profile .profile_features_item_title, 
	.widget_custom_posts_tabs_entries .cmsmasters_tabs a, 
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .color_2, 
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tabs_list_item a, 
	.cmsmasters_price_wrap {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_h6_font_google_font']) . $cmsmasters_option['payday-loans' . '_h6_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_h6_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_h6_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['payday-loans' . '_h6_font_text_decoration'] . ";
	}
	/* Finish H6 Font */


	/* Start Button Font */
	#fbuilder .pbNext, 
	#fbuilder .pbPrevious, 
	#fbuilder .reset-button, 
	.cmsmasters_button, 
	.button, 
	input[type=submit], 
	input[type=button], 
	.widget.widget_wysija input[type=submit], 
	button {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_button_font_google_font']) . $cmsmasters_option['payday-loans' . '_button_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_button_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_button_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_button_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_button_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_button_font_text_transform'] . ";
	}
	
	.widget .cmsmasters_button, 
	.widget .button, 
	.widget input[type=submit], 
	.widget input[type=button], 
	.widget button {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_button_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_button_font_line_height'] - 4) . "px;
	}
	
	#fbuilder input[type=button], 
	#fbuilder .pbNext, 
	#fbuilder .pbPrevious, 
	#fbuilder .reset-button {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_button_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_button_font_line_height'] - 8) . "px;
	}
	
	.gform_wrapper .gform_footer input.button, 
	.gform_wrapper .gform_footer input[type=submit] {
		font-size:" . $cmsmasters_option['payday-loans' . '_button_font_font_size'] . "px !important;
	}
	
	.cmsmasters_button.cmsmasters_but_icon_dark_bg, 
	.cmsmasters_button.cmsmasters_but_icon_light_bg, 
	.cmsmasters_button.cmsmasters_but_icon_divider, 
	.cmsmasters_button.cmsmasters_but_icon_inverse {
		padding-left:" . ((int) $cmsmasters_option['payday-loans' . '_button_font_line_height'] + 20) . "px;
	}
	
	.cmsmasters_button.cmsmasters_but_icon_dark_bg:before, 
	.cmsmasters_button.cmsmasters_but_icon_light_bg:before, 
	.cmsmasters_button.cmsmasters_but_icon_divider:before, 
	.cmsmasters_button.cmsmasters_but_icon_inverse:before, 
	.cmsmasters_button.cmsmasters_but_icon_dark_bg:after, 
	.cmsmasters_button.cmsmasters_but_icon_light_bg:after, 
	.cmsmasters_button.cmsmasters_but_icon_divider:after, 
	.cmsmasters_button.cmsmasters_but_icon_inverse:after {
		width:" . $cmsmasters_option['payday-loans' . '_button_font_line_height'] . "px;
	}
	/* Finish Button Font */


	/* Start Small Text Font */
	small,
	small a,
	#fbuilder .fields span.uh, 
	.cmsmasters_img .cmsmasters_img_caption, 
	.cmsmasters_archive_item_user_name, 
	.cmsmasters_archive_item_user_name a, 
	.cmsmasters_archive_item_category, 
	.cmsmasters_archive_item_category a, 
	.cmsmasters_breadcrumbs_inner, 
	.cmsmasters_breadcrumbs_inner a, 
	.widget_custom_twitter_entries .tweet_time,
	.widget_rss ul li .rss-date, 
	.widget_calendar thead th, 
	.widget_calendar tfoot td a, 
	.widget_calendar caption, 
	.cmsmasters_twitter_wrap .published, 
	#fbuilder label,
	.cmsmasters-form-builder label, 
	.cmsmasters_project_category a,
	.cmsmasters_wrap_pagination ul li, 
	.cmsmasters_wrap_pagination ul li a, 
	.post_nav > span > a, 
	.cmsmasters_archive_type .cmsmasters_archive_item_date_wrap, 
	.cmsmasters_archive_type .cmsmasters_archive_item_date_wrap a, 
	.share_posts .share_posts_inner a, 
	.about_author .about_author_cont > a,
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .published,
	.cmsmasters_comment_item .cmsmasters_comment_item_cont_info > a, 
	.cmsmasters_single_slider .cmsmasters_post_category a, 
	.cmsmasters_comment_item .cmsmasters_comment_item_date, 
	.cmsmasters_slider_post_category a, 
	.cmsmasters_slider_project_category a, 
	.cmsmasters_slider_post_author, 
	.cmsmasters_slider_post_author a, 
	.cmsmasters_slider_project_author, 
	.cmsmasters_slider_project_author a, 
	.cmsmasters_likes a,
	.cmsmasters_comments a,
	.cmsmasters_post_category:before,
	.cmsmasters_post_read_more, 
	.cmsmasters_slider_post_read_more, 
	.cmsmasters_post_cont_info > span,
	.cmsmasters_post_cont_info > span a, 
	form .formError .formErrorContent {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_small_font_google_font']) . $cmsmasters_option['payday-loans' . '_small_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_small_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_small_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_small_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_small_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['payday-loans' . '_small_font_text_transform'] . ";
	}
	
	.cmsmasters_post_default .cmsmasters_post_read_more {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_small_font_font_size'] + 2) . "px;
	}
	
	.cmsmasters_likes a:before, 
	.cmsmasters_comments a:before {
		line-height:" . $cmsmasters_option['payday-loans' . '_small_font_line_height'] . "px;
	}
	
	#fbuilder .fields span.uh, 
	.cmsmasters-form-builder small {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_small_font_font_size'] - 2) . "px;
	}
	
	.cmsmasters_open_profile .profile_details_item_desc, 
	.cmsmasters_open_profile .profile_details_item_desc a, 
	.cmsmasters_open_profile .profile_features_item_desc, 
	.cmsmasters_open_profile .profile_features_item_desc a, 
	.cmsmasters_open_project .project_details_item_desc, 
	.cmsmasters_open_project .project_details_item_desc a, 
	.cmsmasters_open_project .project_features_item_desc, 
	.cmsmasters_open_project .project_features_item_desc a {
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_small_font_line_height'] - 1) . "px;
	}
	
	.gform_wrapper .description, 
	.gform_wrapper .gfield_description, 
	.gform_wrapper .gsection_description, 
	.gform_wrapper .instruction {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_small_font_google_font']) . $cmsmasters_option['payday-loans' . '_small_font_system_font'] . " !important;
		font-size:" . $cmsmasters_option['payday-loans' . '_small_font_font_size'] . "px !important;
		line-height:" . $cmsmasters_option['payday-loans' . '_small_font_line_height'] . "px !important;
	}
	/* Finish Small Text Font */


	/* Start Text Fields Font */
	.widget.widget_wysija input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	textarea,
	select,
	option {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_input_font_google_font']) . $cmsmasters_option['payday-loans' . '_input_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_input_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_input_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_input_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_input_font_font_style'] . ";
	}
	
	.widget input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=search]):not([type=range]),
	.widget textarea {
		font-size:" . ((int) $cmsmasters_option['payday-loans' . '_input_font_font_size'] - 2)  . "px;
	}
	
	.gform_wrapper input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	.gform_wrapper textarea, 
	.gform_wrapper select {
		font-size:" . $cmsmasters_option['payday-loans' . '_input_font_font_size'] . "px !important;
	}
	
	.header_mid input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]) {
		font-size:60px; /* static */
		line-height:70px; /* static */
	}
	
	@media only screen and (max-width: 1024px) {
		.header_mid input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]) {
			font-size:40px; /* static */
			line-height:50px; /* static */
		}
	}
	
	/* Finish Text Fields Font */


	/* Start Blockquote Font */
	blockquote,
	.cmsmasters_quotes_grid .cmsmasters_quote_content, 
	.cmsmasters_quotes_slider .cmsmasters_quote_content {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_quote_font_google_font']) . $cmsmasters_option['payday-loans' . '_quote_font_system_font'] . ";
		font-size:" . $cmsmasters_option['payday-loans' . '_quote_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['payday-loans' . '_quote_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['payday-loans' . '_quote_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_quote_font_font_style'] . ";
	}
	
	.cmsmasters_quotes_grid .cmsmasters_quote_content {
		font-size:" .  ((int) $cmsmasters_option['payday-loans' . '_quote_font_font_size'] - 4) . "px;
		line-height:" . ((int) $cmsmasters_option['payday-loans' . '_quote_font_line_height'] - 10) . "px;
	}
	
	q {
		font-family:" . payday_loans_get_google_font($cmsmasters_option['payday-loans' . '_quote_font_google_font']) . $cmsmasters_option['payday-loans' . '_quote_font_system_font'] . ";
		font-weight:" . $cmsmasters_option['payday-loans' . '_quote_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['payday-loans' . '_quote_font_font_style'] . ";
	}
	/* Finish Blockquote Font */

/***************** Finish Theme Font Styles ******************/


";


if (CMSMASTERS_DONATIONS) {

	$custom_css .= "
/***************** Start CMSMASTERS Donations Font Styles ******************/

	/* Start Content Font */
	/* Finish Content Font */
	
	
	/* Start Link Font */
	/* Finish Link Font */
	
	
	/* Start Navigation Title Font */
	/* Finish Navigation Title Font */
	
	
	/* Start H1 Font */
	/* Finish H1 Font */
	
	
	/* Start H2 Font */
	/* Finish H2 Font */
	
	
	/* Start H3 Font */
	/* Finish H3 Font */
	
	
	/* Start H4 Font */
	/* Finish H4 Font */
	
	
	/* Start H5 Font */
	/* Finish H5 Font */
	
	
	/* Start H6 Font */
	/* Finish H6 Font */
	
	
	/* Start Button Font */
	/* Finish Button Font */
	
	
	/* Start Small Text Font */
	/* Finish Small Text Font */

/***************** Finish CMSMASTERS Donations Font Styles ******************/


";

}
	
	return apply_filters('payday_loans_theme_fonts_filter', $custom_css);
}

