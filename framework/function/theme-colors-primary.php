<?php
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version 	1.0.8
 * 
 * Theme Primary Color Schemes Rules
 * Created by CMSMasters
 * 
 */


function payday_loans_theme_colors_primary() {
	$cmsmasters_option = payday_loans_get_global_options();
	
	
	$cmsmasters_color_schemes = cmsmasters_color_schemes_list();
	
	
	$custom_css = "/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version 	1.0.8
 * 
 * Theme Primary Color Schemes Rules
 * Created by CMSMasters
 * 
 */

";
	
	
	foreach ($cmsmasters_color_schemes as $scheme => $title) {
		$rule = (($scheme != 'default') ? "html .cmsmasters_color_scheme_{$scheme} " : '');
		
		
		$custom_css .= "
/***************** Start {$title} Color Scheme Rules ******************/

	/* Start Main Content Font Color */
	" . (($scheme == 'default') ? "body," : '') . "
	" . (($scheme != 'default') ? ".cmsmasters_color_scheme_{$scheme}," : '') . "
	{$rule}input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]):focus,
	{$rule}.page #fbuilder .codepeoplecalculatedfield, 
	{$rule}.post_nav > span a,
	{$rule}.color_2,
	{$rule}.cmsmasters_archive_type .cmsmasters_archive_item_date_wrap:before, 
	{$rule}.img_placeholder:before,
	{$rule}.cmsmasters_notice .notice_close, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_date,
	{$rule}.cmsmasters_wrap_pagination ul li, 
	{$rule}.cmsmasters_wrap_pagination ul li a, 
	{$rule}.cmsmasters_comments > a,
	{$rule}.cmsmasters_likes > a,
	{$rule}.cmsmasters_comments > a:hover,
	{$rule}.cmsmasters_likes > a:hover,
	{$rule}textarea:focus,
	{$rule}select,
	{$rule}option {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_color']) . "
	}
	
	{$rule}input:focus::-webkit-input-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_color']) . "
	}
	
	{$rule}input:focus::-moz-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_color']) . "
	}
	
	{$rule}textarea:focus::-moz-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_color']) . "
	}
	
	{$rule}textarea:focus::-webkit-input-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_color']) . "
	}
	
	/* Finish Main Content Font Color */
	
	
	/* Start Primary Color */
	{$rule}a,
	" . (($scheme != 'default') ? ".cmsmasters_color_scheme_{$scheme}.cmsmasters_footer_default .footer_custom_html," : '') . "
	{$rule}.cmsmasters_dropcap.type1,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_top:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_heading_left .icon_box_heading:before,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_icon:before,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_horizontal .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner:before, 
	{$rule}.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner:before, 
	{$rule}.bypostauthor > .comment-body .alignleft:before,
	{$rule}.cmsmasters_post_default .cmsmasters_post_comments > a:hover:before,
	{$rule}.cmsmasters_post_default .cmsmasters_post_likes > a:hover:before,
	{$rule}.cmsmasters_post_default .cmsmasters_post_likes > a.active:before,
	{$rule}.cmsmasters_open_post .cmsmasters_post_comments > a:hover:before,
	{$rule}.cmsmasters_open_post .cmsmasters_post_likes > a:hover:before,
	{$rule}.cmsmasters_open_post .cmsmasters_post_likes > a.active:before,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > a:hover,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category > li > a:hover,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > ul li a:hover, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_archive li, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_archive li a:hover, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_cont_info > a:hover, 
	{$rule}.cmsmasters_post_cont_info > span a:hover,
	{$rule}.widget_custom_twitter_entries .tweet_time:before, 
	{$rule}.widget_custom_twitter_entries ul li a, 
	{$rule}.cmsmasters_post_read_more:hover, 
	{$rule}.cmsmasters_slider_post_read_more:hover, 
	{$rule}.cmsmasters_project_category a:hover, 
	{$rule}.profile_social_icons_list .cmsmasters_social_icon, 
	{$rule}.share_posts .share_posts_inner a:hover,
	{$rule}.cmsmasters_slider_post_category a:hover, 
	{$rule}.cmsmasters_slider_post_author a:hover, 
	{$rule}.cmsmasters_single_slider .cmsmasters_post_category a:hover,
	{$rule}.cmsmasters_archive_type .cmsmasters_archive_item_date_wrap a:hover,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_horizontal .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_title:before,
	{$rule}.cmsmasters_comments > a:before,
	{$rule}.cmsmasters_likes > a:before,
	{$rule}.cmsmasters_quote_title,
	{$rule}.cmsmasters_archive_item_user_name a:hover, 
	{$rule}.cmsmasters_archive_item_category a:hover, 
	{$rule}.widget a:hover, 
	{$rule}blockquote:before, 
	{$rule}.widget_custom_contact_info_entries > *:before, 
	{$rule}.widget_calendar tbody #today, 
	{$rule}.cmsmasters_twitter_wrap .published:before, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote_content:before,
	{$rule}.cmsmasters_quotes_slider .quote_placeholder:before,
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_edit a, 
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_meta a {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	" . (($scheme == 'default') ? "#slide_top," : '') . "
	" . (($scheme == 'default') ? "mark," : '') . "
	" . (($scheme != 'default') ? ".cmsmasters_color_scheme_{$scheme} mark," : '') . "
	{$rule}.cmsmasters_dropcap.type2,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_top:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left_top:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left:before,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_bg .cmsmasters_icon_list_item .cmsmasters_icon_list_icon,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_item:hover .cmsmasters_icon_list_icon,
	{$rule}.cmsmasters_post_date, 
	{$rule}.comment-form .form-submit input[type=submit]:hover, 
	{$rule}.cmsmasters_slider_post_date, 
	{$rule}.cmsmasters_button, 
	{$rule}.button, 
	{$rule}input[type=submit], 
	{$rule}input[type=button], 
	{$rule}.page #fbuilder fieldset .pbNext, 
	{$rule}.page #fbuilder fieldset .pbPrevious, 
	{$rule}.page #fbuilder fieldset .reset-button, 
	{$rule}.widget_tag_cloud a, 
	{$rule}.cmsmasters_post_timeline:hover .cmsmasters_post_date, 
	{$rule}.cmsmasters_profile_horizontal .profile_outer:hover .profile_inner, 
	{$rule}.cmsmasters_img_rollover .cmsmasters_open_post_link:hover > span:after, 
	{$rule}.cmsmasters_img_rollover .cmsmasters_open_post_link:hover > span:before, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_slider_project_inner, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .project_inner, 
	{$rule}.cmsmasters_items_sort_block .button, 
	{$rule}.owl-pagination .owl-page:hover,
	{$rule}.owl-pagination .owl-page.active,
	{$rule}.cmsmasters_quotes_slider_wrap .owl-pagination .owl-page:hover, 
	{$rule}.cmsmasters_quotes_slider_wrap .owl-pagination .owl-page.active, 
	{$rule}.cmsmasters_pricing_item.pricing_best .cmsmasters_pricing_item_inner, 
	{$rule}.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner:before, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=checkbox] + span.wpcf7-list-item-label:after, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=checkbox] + label:after, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=radio] + span.wpcf7-list-item-label:after, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=radio] + label:after, 
	{$rule}#fbuilder input[type=radio] + .field_before:after,
	{$rule}#fbuilder input[type=checkbox] + .field_before:after,
	{$rule}.cmsmasters_open_post .cmsmasters_post_tags > a, 
	{$rule}.cmsmasters_social_icon {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}textarea:focus, 
	{$rule}.page #fbuilder .codepeoplecalculatedfield, 
	{$rule}input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]):focus {
		background-color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . ", 0.40);
	}
	
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_border .cmsmasters_icon_list_item .cmsmasters_icon_list_icon:after, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner:before {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_slider_project_inner:before, 
	{$rule}.cmsmasters_profile_horizontal .profile_outer:hover .profile_inner:before, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .project_inner:before {
		" . cmsmasters_color_css('border-bottom-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_post_timeline.cmsmasters_timeline_left:hover .cmsmasters_post_date:before {
		" . cmsmasters_color_css('border-right-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_post_timeline.cmsmasters_timeline_right:hover .cmsmasters_post_date:before {
		" . cmsmasters_color_css('border-left-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	/* Finish Primary Color */
	
	
	/* Start Highlight Color */
	{$rule}a:hover,
	{$rule}h1 a:hover,
	{$rule}h2 a:hover,
	{$rule}h3 a:hover,
	{$rule}h4 a:hover,
	{$rule}h5 a:hover,
	{$rule}h6 a:hover,
	{$rule}blockquote, 
	{$rule}.widget a, 
	{$rule}.cmsmasters_profile_subtitle,
	{$rule}.owl-buttons > div:hover, 
	{$rule}.post_nav > span a:hover, 
	{$rule}.cmsmasters_items_filter_list li a, 
	{$rule}.cmsmasters_quote_content, 
	{$rule}.cmsmasters_single_slider .cmsmasters_post_category a, 
	{$rule}.post_nav > span a:hover + span > span:before, 
	{$rule}.cmsmasters_post_read_more, 
	{$rule}.cmsmasters_slider_post_read_more, 
	{$rule}.share_posts .share_posts_inner a, 
	{$rule}.cmsmasters_post_cont_info > span a, 
	{$rule}.cmsmasters_project_category, 
	{$rule}.cmsmasters_project_category a, 
	{$rule}.cmsmasters_slider_post_category a, 
	{$rule}.cmsmasters_slider_project_category a, 
	{$rule}.cmsmasters_slider_post_author a, 
	{$rule}.cmsmasters_slider_project_author a, 
	{$rule}.cmsmasters_comments > a:hover:before, 
	{$rule}.cmsmasters_likes > a:hover:before, 
	{$rule}.cmsmasters_likes > a.active:before, 
	{$rule}.widget_custom_twitter_entries ul li a:hover, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > a,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > ul li a:before,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category > li > a,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category > li > ul li a:before,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_archive > li a:before, 
	{$rule}.widgettitle a:hover, 
	{$rule}.widget_custom_twitter_entries .tweet_time, 
	{$rule}.widget_custom_contact_info_entries, 
	{$rule}.search_bar_wrap .search_button button:hover,
	{$rule}.widget_calendar thead th,
	{$rule}.cmsmasters_archive_type .cmsmasters_archive_item_type,
	{$rule}.widget_nav_menu li a:hover,
	{$rule}.cmsmasters_archive_item_user_name a, 
	{$rule}.cmsmasters_archive_item_category a, 
	{$rule}.cmsmasters_wrap_more_items.cmsmasters_loading:before,
	{$rule}.cmsmasters_archive_type .cmsmasters_archive_item_date_wrap, 
	{$rule}.cmsmasters_archive_type .cmsmasters_archive_item_date_wrap a, 
	{$rule}.cmsmasters_tabs .cmsmasters_tabs_list_item a:hover, 
	{$rule}.cmsmasters_tabs .cmsmasters_tabs_list_item.current_tab a, 
	{$rule}.cmsmasters_wrap_pagination ul li a:hover, 
	{$rule}.cmsmasters_wrap_pagination ul li .current, 
	{$rule}.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner:before,
	{$rule}.cmsmasters_post_default.sticky .cmsmasters_sticky, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > a:hover,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > ul li a, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_archive li, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_archive li a, 
	{$rule}.cmsmasters_toggles .current_toggle .cmsmasters_toggle_title a, 
	{$rule}.cmsmasters_toggles .cmsmasters_toggle_title a:hover, 
	{$rule}.cmsmasters_twitter_wrap .cmsmasters_twitter_item_content, 
	{$rule}.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner:before, 
	{$rule}.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat_counter_wrap, 
	{$rule}.cmsmasters_profile_vertical .profile_social_icons_list a:hover, 
	{$rule}.cmsmasters_open_profile .profile_social_icons_list a:hover, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_cont_info > a, 
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_edit a:hover, 
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_meta a:hover {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . "
	}
	
	" . (($scheme == 'default') ? "#slide_top:hover," : '') . "
	{$rule}.cmsmasters_post_default .cmsmasters_post_info_bot, 
	{$rule}.cmsmasters_post_timeline .cmsmasters_post_date, 
	{$rule}.cmsmasters_button:hover, 
	{$rule}.button:hover, 
	{$rule}.button.current, 
	{$rule}.button.active, 
	{$rule}input[type=submit]:hover, 
	{$rule}input[type=button]:hover, 
	{$rule}button:hover, 
	{$rule}.page #fbuilder fieldset .pbNext:hover, 
	{$rule}.page #fbuilder fieldset .pbPrevious:hover, 
	{$rule}.page #fbuilder fieldset .reset-button:hover, 
	{$rule}.comment-form .form-submit input[type=submit], 
	{$rule}.headline_outer, 
	{$rule}.widget_tag_cloud a:hover, 
	{$rule}.cmsmasters_table_row_header, 
	{$rule}.cmsmasters_toggles .current_toggle .cmsmasters_toggle_plus span, 
	{$rule}.cmsmasters_toggles .cmsmasters_toggle_title:hover .cmsmasters_toggle_plus span, 
	{$rule}.cmsmasters_toggles .cmsmasters_toggle_plus span, 
	{$rule}.pricing_best .cmsmasters_pricing_but_wrap .cmsmasters_button:hover, 
	{$rule}.cmsmasters_pricing_item .cmsmasters_pricing_item_head, 
	{$rule}.cmsmasters_notice .notice_close:hover, 
	{$rule}.cmsmasters_content_slider .owl-buttons > div:hover:before, 
	{$rule}.cmsmasters_items_filter_list li a:hover, 
	{$rule}.cmsmasters_items_filter_list li.current a, 
	{$rule}.cmsmasters_items_sort_block .button:hover, 
	{$rule}.cmsmasters_items_sort_block .button.current, 
	{$rule}.cmsmasters_open_post .cmsmasters_open_post_info_bot, 
	{$rule}.cmsmasters_open_post .cmsmasters_post_tags > a:hover, 
	{$rule}.cmsmasters_social_icon:hover {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsmasters_pricing_item .cmsmasters_pricing_item_head:before {
		" . cmsmasters_color_css('border-top-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.header_mid .search_wrap .search_bar_wrap .search_field {
		background-color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . ", 0.95);
	}
	
	{$rule}.cmsmasters_profile_horizontal .profile_outer:hover .img_placeholder, 
	{$rule}.cmsmasters_hover_slider .cmsmasters_hover_slider_thumbs li.hovered_item a:before, 
	{$rule}.cmsmasters_img_rollover_wrap .cmsmasters_img_rollover,
	{$rule}.cmsmasters_img_wrap > a:before {
		background-color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . ", 0.75);
	}
	
	{$rule}.cmsmasters_post_timeline.cmsmasters_timeline_left .cmsmasters_post_date:before {
		" . cmsmasters_color_css('border-right-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsmasters_post_timeline.cmsmasters_timeline_right .cmsmasters_post_date:before {
		" . cmsmasters_color_css('border-left-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsmasters_content_slider .owl-buttons > div:before {
		background-color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . ", 0.20);
	}
	
	/* Finish Highlight Color */
	
	
	/* Start Headings Color */
	" . (($scheme == 'default') ? ".headline_outer," : '') . "
	" . (($scheme == 'default') ? ".headline_outer a:hover," : '') . "
	{$rule}h1,
	{$rule}h2,
	{$rule}h3,
	{$rule}h4,
	{$rule}h5,
	{$rule}h6,
	{$rule}h1 a,
	{$rule}h2 a,
	{$rule}h3 a,
	{$rule}h4 a,
	{$rule}h5 a,
	{$rule}h6 a,
	{$rule}fieldset legend,
	{$rule}blockquote footer,
	{$rule}table caption,
	{$rule}.cmsmasters_quote_subtitle,
	{$rule}.img_placeholder_small, 
	{$rule}.cmsmasters_toggles .cmsmasters_toggle_title a, 
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_horizontal .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat_title,
	{$rule}.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat_title, 
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_title_counter_wrap, 
	{$rule}.pricing_best .cmsmasters_pricing_but_wrap .cmsmasters_button, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_title, 
	{$rule}.cmsmasters_table_row_footer td,
	{$rule}.widget_nav_menu li a,
	{$rule}.widgettitle,
	{$rule}.widgettitle a,
	{$rule}.cmsmasters_img .cmsmasters_img_caption, 
	{$rule}.widget_rss ul li .rss-date, 
	{$rule}.search_bar_wrap .search_button button,
	{$rule}.cmsmasters_twitter_wrap .published,
	{$rule}.cmsmasters_tabs .cmsmasters_tabs_list_item a {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.cmsmasters_icon_list_items .cmsmasters_icon_list_item .cmsmasters_icon_list_icon,
	{$rule}.cmsmasters_toggles .cmsmasters_toggle_plus span,
	{$rule}form .formError .formErrorContent {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_heading']) . "
	}
	/* Finish Headings Color */
	
	
	/* Start Main Background Color */
	{$rule}mark,
	{$rule}form .formError .formErrorContent,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left_top:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_top:before,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_border .cmsmasters_icon_list_item .cmsmasters_icon_list_icon:before,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner, 
	{$rule}.cmsmasters_post_date, 
	{$rule}.cmsmasters_slider_post_date, 
	{$rule}.cmsmasters_button, 
	{$rule}.button, 
	{$rule}input[type=submit], 
	{$rule}input[type=button], 
	{$rule}button, 
	{$rule}.page #fbuilder fieldset .pbNext, 
	{$rule}.page #fbuilder fieldset .pbPrevious, 
	{$rule}.page #fbuilder fieldset .reset-button, 
	{$rule}.page #fbuilder fieldset .pbNext:hover, 
	{$rule}.page #fbuilder fieldset .pbPrevious:hover, 
	{$rule}.page #fbuilder fieldset .reset-button:hover, 
	{$rule}.widget .cmsmasters_button, 
	{$rule}.widget .button, 
	{$rule}.widget input[type=submit], 
	{$rule}.widget input[type=button], 
	{$rule}.widget button, 
	{$rule}.cmsmasters_button:hover, 
	{$rule}.button:hover, 
	{$rule}input[type=submit]:hover, 
	{$rule}input[type=button]:hover, 
	{$rule}button:hover, 
	{$rule}.widget .cmsmasters_button:hover, 
	{$rule}.widget .button:hover, 
	{$rule}.widget input[type=submit]:hover, 
	{$rule}.widget input[type=button]:hover, 
	{$rule}.widget button:hover,
	{$rule}.cmsmasters_pricing_table .pricing_title, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_price_wrap, 
	{$rule}.cmsmasters_social_icon:hover, 
	{$rule}.cmsmasters_items_sort_block .button, 
	{$rule}.cmsmasters_content_slider .owl-buttons > div, 
	{$rule}.cmsmasters_items_filter_list li a:hover, 
	{$rule}.cmsmasters_items_filter_list li.current a, 
	{$rule}.cmsmasters_pricing_table .pricing_best .feature_list, 
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_bars_custom_color:before, 
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_bars_custom_color .cmsmasters_stat_counter_wrap, 
	{$rule}.pricing_best .cmsmasters_pricing_but_wrap .cmsmasters_button:hover, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .cmsmasters_project_header a, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .cmsmasters_project_category, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .cmsmasters_project_category a, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .cmsmasters_project_content, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .cmsmasters_comments a, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .cmsmasters_likes a, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .cmsmasters_comments a:before, 
	{$rule}.cmsmasters_project_grid .project_outer:hover .cmsmasters_likes a:before, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_slider_project_header a, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_slider_project_category, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_slider_project_category a, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_slider_project_content, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_comments a, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_likes a, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_comments a:before, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_outer:hover .cmsmasters_likes a:before, 
	{$rule}.cmsmasters_profile_horizontal .profile_outer:hover .cmsmasters_profile_title, 
	{$rule}.cmsmasters_profile_horizontal .profile_outer:hover .cmsmasters_profile_title a, 
	{$rule}.cmsmasters_profile_horizontal .profile_outer:hover .cmsmasters_profile_subtitle, 
	{$rule}.cmsmasters_profile_horizontal .profile_outer:hover .cmsmasters_profile_content, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_project_header a, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_project_category, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_project_category a, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_likes a, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_comments a, 	
	{$rule}.cmsmasters_project_puzzle .cmsmasters_likes a:before, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_comments a:before, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_comments > a,
	{$rule}.cmsmasters_post_default .cmsmasters_post_likes > a,
	{$rule}.cmsmasters_post_default .cmsmasters_post_comments > a:before,
	{$rule}.cmsmasters_post_default .cmsmasters_post_likes > a:before,
	{$rule}.cmsmasters_post_default .cmsmasters_post_comments > a:hover,
	{$rule}.cmsmasters_post_default .cmsmasters_post_likes > a:hover,
	{$rule}.cmsmasters_open_post .cmsmasters_post_comments > a,
	{$rule}.cmsmasters_open_post .cmsmasters_post_likes > a,
	{$rule}.cmsmasters_open_post .cmsmasters_post_comments > a:before,
	{$rule}.cmsmasters_open_post .cmsmasters_post_likes > a:before,
	{$rule}.cmsmasters_open_post .cmsmasters_post_comments > a:hover,
	{$rule}.cmsmasters_open_post .cmsmasters_post_likes > a:hover,
	{$rule}.cmsmasters_open_post .cmsmasters_post_tags > a, 
	{$rule}.profile_social_icons_list .cmsmasters_social_icon:hover, 
	{$rule}.widget_tag_cloud a, 
	{$rule}.error .error_title, 
	{$rule}.cmsmasters_breadcrumbs_inner, 
	{$rule}.cmsmasters_breadcrumbs_inner a, 
	{$rule}.cmsmasters_breadcrumbs_inner a:hover, 
	{$rule}.headline_text .entry-title, 
	{$rule}.widget_tag_cloud a:hover, 
	{$rule}.cmsmasters_notice .notice_close:hover, 
	{$rule}.cmsmasters_table_row_header th, 
	{$rule}.cmsmasters_social_icon {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	" . (($scheme == 'default') ? "body," : '') . "
	" . (($scheme != 'default') ? ".cmsmasters_color_scheme_{$scheme}," : '') . "
	" . (($scheme == 'default') ? ".middle_inner," : '') . "
	" . (($scheme != 'default') ? ".cmsmasters_color_scheme_{$scheme}.cmsmasters_footer_default .footer_copyright," : '') . "
	{$rule}.cmsmasters_img_rollover .cmsmasters_open_post_link > span:after, 
	{$rule}.cmsmasters_img_rollover .cmsmasters_open_post_link > span:before, 
	{$rule}.owl-pagination .owl-page, 
	{$rule}.cmsmasters_single_slider .cmsmasters_owl_slider .owl-buttons > div, 
	{$rule}.cmsmasters_posts_slider .owl-buttons > div, 
	{$rule}.cmsmasters_twitter_wrap .owl-buttons, 
	{$rule}.cmsmasters_notice .notice_close, 
	{$rule}.cmsmasters_toggles.toggles_mode_toggle .cmsmasters_toggle_title, 
	{$rule}.pricing_best .cmsmasters_pricing_but_wrap .cmsmasters_button, 
	{$rule}option {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_wrap_pagination ul li .current:before, 
	{$rule}.cmsmasters_toggles.toggles_mode_toggle .cmsmasters_toggle_title:before, 
	{$rule}.post_nav:before {
		" . cmsmasters_color_css('border-top-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_tabs.tabs_mode_tab .cmsmasters_tabs_list_item {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_breadcrumbs_wrapper {
		border-color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . ", 0.10);
	}
	
	/* Finish Main Background Color */
	
	
	/* Start Alternate Background Color */
	" . (($scheme == 'default') ? "#slide_top," : '') . "
	{$rule}.cmsmasters_dropcap.type2,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_icon_wrap, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner:before {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}select,
	{$rule}fieldset,
	{$rule}table, 
	{$rule}fieldset legend,
	{$rule}.img_placeholder_small, 
	{$rule}.cmsmasters_featured_block,
	{$rule}textarea, 
	{$rule}input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	" . (($scheme != 'default') ? ".cmsmasters_color_scheme_{$scheme}.cmsmasters_footer_default," : '') . "
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_top,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_icon,
	{$rule}.gallery-item .gallery-icon,
	{$rule}.cmsmasters_post_masonry .cmsmasters_post_cont,
	{$rule}.post_nav,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_horizontal .cmsmasters_stat_wrap:before,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat_inner,
	{$rule}.cmsmasters_project_grid .project_outer .project_inner,
	{$rule}.cmsmasters_items_filter_list li a, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_content,
	{$rule}.cmsmasters_single_slider .cmsmasters_single_slider_item_inner,
	{$rule}.cmsmasters_post_timeline .cmsmasters_post_cont_inner,
	{$rule}.cmsmasters_profile_horizontal .profile_inner, 
	{$rule}.gallery-item .gallery-caption,
	{$rule}.cmsmasters_img.with_caption, 
	{$rule}.cmsmasters_wrap_pagination, 
	{$rule}.cmsmasters_slider_project_inner, 
	{$rule}.cmsmasters_table_row_footer, 
	{$rule}.cmsmasters_slider_post_inner, 
	{$rule}.widget_nav_menu .menu li a, 
	{$rule}.cmsmasters_post_default.sticky .cmsmasters_sticky, 
	{$rule}.cmsmasters_tabs .cmsmasters_tabs_list_item, 
	{$rule}.cmsmasters_toggles.toggles_mode_accordion .cmsmasters_toggle_title, 
	{$rule}.cmsmasters_toggles.toggles_mode_toggle .cmsmasters_toggle_inner, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote, 
	{$rule}.cmsmasters_pricing_item .cmsmasters_pricing_item_inner, 
	{$rule}.cmsmasters_notice.cmsmasters_notice_custom, 
	{$rule}#fbuilder input[type=radio] + .field_before:before,
	{$rule}#fbuilder input[type=checkbox] + .field_before:before,
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=checkbox] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=checkbox] + label:before, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=radio] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=radio] + label:before {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.l_sidebar .widget_nav_menu .menu li.current_page_item:before, 
	{$rule}.cmsmasters_tabs.tabs_mode_tour .cmsmasters_tabs_list_item:before {
		" . cmsmasters_color_css('border-left-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.widget_nav_menu .menu li:before, 
	{$rule}.cmsmasters_tabs.tabs_mode_tour.tabs_pos_right .cmsmasters_tabs_list_item:before {
		" . cmsmasters_color_css('border-right-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_alternate']) .  "
	}
	
	{$rule}.cmsmasters_toggles.toggles_mode_accordion .cmsmasters_toggle_title:before, 
	{$rule}.cmsmasters_tabs.tabs_mode_tab .cmsmasters_tabs_list_item:before, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_content:before {
		" . cmsmasters_color_css('border-top-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_inner:before, 
	{$rule}.cmsmasters_profile_horizontal .profile_inner:before, 
	{$rule}.cmsmasters_project_grid .project_outer .project_inner:before {
		" . cmsmasters_color_css('border-bottom-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_alternate']) . "
	}
	
	/* Finish Alternate Background Color */
	
	
	/* Start Borders Color */
	{$rule}.widget_wysija_cont p label, 
	{$rule}input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	{$rule}textarea, 
	{$rule}.owl-buttons > div {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}input::-webkit-input-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}input::-moz-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}textarea::-moz-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}textarea::-webkit-input-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.blog.timeline:before, 
	{$rule}.cmsmasters_clients_slider_wrap .owl-page, 
	{$rule}.cmsmasters_quotes_slider_wrap .owl-pagination .owl-page, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category:before, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_archive:before, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li:before {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_info, 
	{$rule}hr,
	{$rule}.cmsmasters_divider,
	{$rule}.cmsmasters_table tbody tr,
	{$rule}.cmsmasters_widget_divider,
	{$rule}.cmsmasters_icon_wrap .cmsmasters_simple_icon, 
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_top,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_icon_list_type_block .cmsmasters_icon_list_item,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_bg .cmsmasters_icon_list_icon:after,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_icon:after,
	{$rule}#fbuilder input[type=radio] + .field_before:before,
	{$rule}#fbuilder input[type=checkbox] + .field_before:before, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=checkbox] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=checkbox] + label:before, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=radio] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=radio] + label:before,
	{$rule}table,
	{$rule}table tr,
	{$rule}table td,
	{$rule}table th {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_border']) . "
	}
	/* Finish Borders Color */
	
	
	/* Start Custom Rules */
	{$rule}::selection {
		" . cmsmasters_color_css('background', $cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . ";
	}
	
	{$rule}::-moz-selection {
		" . cmsmasters_color_css('background', $cmsmasters_option['payday-loans' . '_' . $scheme . '_hover']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_table, 
	{$rule}.profile_social_icons_list .cmsmasters_social_icon:hover,
	{$rule}.profile_social_icons_list .cmsmasters_social_icon {
		background:transparent;
	}
	";
	
	
	if ($scheme != 'default') {
		$custom_css .= "
		.cmsmasters_color_scheme_{$scheme}.cmsmasters_row_top_zigzag:before, 
		.cmsmasters_color_scheme_{$scheme}.cmsmasters_row_bot_zigzag:after {
			background-image: -webkit-linear-gradient(135deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					-webkit-linear-gradient(45deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
			background-image: -moz-linear-gradient(135deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					-moz-linear-gradient(45deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
			background-image: -ms-linear-gradient(135deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					-ms-linear-gradient(45deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
			background-image: -o-linear-gradient(135deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					-o-linear-gradient(45deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
			background-image: linear-gradient(315deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					linear-gradient(45deg, " . $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
		}
		";
	}
	
	
	$custom_css .= "
	/* Finish Custom Rules */

/***************** Finish {$title} Color Scheme Rules ******************/


/***************** Start {$title} Button Color Scheme Rules ******************/
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_hover:hover, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_bg_hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_left, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_right, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_top, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_bottom, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_vert, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_hor, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_diag {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_left:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_right:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_top:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_bottom:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_vert:hover, 
	{$rule}.cmsmasters_button.cm.sms_but_bg_expand_hor:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_diag:hover, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_bg_slide_left, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_bg_slide_right, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_bg_slide_top, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_bg_slide_bottom, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_bg_expand_vert, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_bg_expand_hor, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_bg_expand_diag {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_left:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_right:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_top:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_bottom:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_vert:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_hor:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_diag:after {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_shadow {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_shadow:hover, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_shadow {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_dark_bg, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_light_bg, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_divider {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_dark_bg:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_light_bg:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_divider:hover, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_dark_bg, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_light_bg, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_divider {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_divider:after {
		" . cmsmasters_color_css('border-right-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:before {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:after {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:hover, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_inverse {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:hover:before, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_inverse:before {
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:hover:after, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_inverse:after {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_slide_left, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_slide_right {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_slide_left:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_slide_right:hover, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_slide_left, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_slide_right {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_left, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_right, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_top, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_bottom {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_left:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_right:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_top:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_bottom:hover, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_hover_slide_left, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_hover_slide_right, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_hover_slide_top, 
	{$rule}.cmsmasters_paypal_donations > form:hover + .cmsmasters_button.cmsmasters_but_icon_hover_slide_bottom {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['payday-loans' . '_' . $scheme . '_bg']) . "
	}

/***************** Finish {$title} Button Color Scheme Rules ******************/


";
	}
	
	
	return apply_filters('payday_loans_theme_colors_primary_filter', $custom_css);
}

