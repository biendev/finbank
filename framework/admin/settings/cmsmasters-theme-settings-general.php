<?php 
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version 	1.0.6
 * 
 * Admin Panel General Options
 * Created by CMSMasters
 * 
 */


function payday_loans_options_general_tabs() {
	$cmsmasters_option = payday_loans_get_global_options();
	
	$tabs = array();
	
	$tabs['general'] = esc_attr__('General', 'payday-loans');
	
	if ($cmsmasters_option['payday-loans' . '_theme_layout'] === 'boxed') {
		$tabs['bg'] = esc_attr__('Background', 'payday-loans');
	}
	
	$tabs['header'] = esc_attr__('Header', 'payday-loans');
	$tabs['content'] = esc_attr__('Content', 'payday-loans');
	$tabs['footer'] = esc_attr__('Footer', 'payday-loans');
	
	return $tabs;
}


function payday_loans_options_general_sections() {
	$tab = payday_loans_get_the_tab();
	
	switch ($tab) {
	case 'general':
		$sections = array();
		
		$sections['general_section'] = esc_attr__('General Options', 'payday-loans');
		
		break;
	case 'bg':
		$sections = array();
		
		$sections['bg_section'] = esc_attr__('Background Options', 'payday-loans');
		
		break;
	case 'header':
		$sections = array();
		
		$sections['header_section'] = esc_attr__('Header Options', 'payday-loans');
		
		break;
	case 'content':
		$sections = array();
		
		$sections['content_section'] = esc_attr__('Content Options', 'payday-loans');
		
		break;
	case 'footer':
		$sections = array();
		
		$sections['footer_section'] = esc_attr__('Footer Options', 'payday-loans');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return $sections;
} 


function payday_loans_options_general_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = payday_loans_get_the_tab();
	}
	
	$options = array();
	
	switch ($tab) {
	case 'general':
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'payday-loans' . '_theme_layout', 
			'title' => esc_html__('Theme Layout', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'liquid', 
			'choices' => array( 
				esc_html__('Liquid', 'payday-loans') . '|liquid', 
				esc_html__('Boxed', 'payday-loans') . '|boxed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'payday-loans' . '_logo_type', 
			'title' => esc_html__('Logo Type', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'image', 
			'choices' => array( 
				esc_html__('Image', 'payday-loans') . '|image', 
				esc_html__('Text', 'payday-loans') . '|text' 
			) 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'payday-loans' . '_logo_url', 
			'title' => esc_html__('Logo Image', 'payday-loans'), 
			'desc' => esc_html__('Choose your website logo image.', 'payday-loans'), 
			'type' => 'upload', 
			'std' => '|' . get_template_directory_uri() . '/img/logo.png', 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'payday-loans' . '_logo_url_retina', 
			'title' => esc_html__('Retina Logo Image', 'payday-loans'), 
			'desc' => esc_html__('Choose logo image for retina displays.', 'payday-loans'), 
			'type' => 'upload', 
			'std' => '|' . get_template_directory_uri() . '/img/logo_retina.png', 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'payday-loans' . '_logo_title', 
			'title' => esc_html__('Logo Title', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => ((get_bloginfo('name')) ? get_bloginfo('name') : 'Payday Loans'), 
			'class' => 'nohtml' 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'payday-loans' . '_logo_subtitle', 
			'title' => esc_html__('Logo Subtitle', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => '', 
			'class' => 'nohtml' 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'payday-loans' . '_logo_custom_color', 
			'title' => esc_html__('Custom Text Colors', 'payday-loans'), 
			'desc' => esc_html__('enable', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'payday-loans' . '_logo_title_color', 
			'title' => esc_html__('Logo Title Color', 'payday-loans'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => '' 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'payday-loans' . '_logo_subtitle_color', 
			'title' => esc_html__('Logo Subtitle Color', 'payday-loans'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => '' 
		);
		
		break;
	case 'bg':
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'payday-loans' . '_bg_col', 
			'title' => esc_html__('Background Color', 'payday-loans'), 
			'desc' => '', 
			'type' => 'color', 
			'std' => '#ffffff' 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'payday-loans' . '_bg_img_enable', 
			'title' => esc_html__('Background Image Visibility', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'payday-loans' . '_bg_img', 
			'title' => esc_html__('Background Image', 'payday-loans'), 
			'desc' => esc_html__('Choose your custom website background image url.', 'payday-loans'), 
			'type' => 'upload', 
			'std' => '', 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'payday-loans' . '_bg_rep', 
			'title' => esc_html__('Background Repeat', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'no-repeat', 
			'choices' => array( 
				esc_html__('No Repeat', 'payday-loans') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'payday-loans') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'payday-loans') . '|repeat-y', 
				esc_html__('Repeat', 'payday-loans') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'payday-loans' . '_bg_pos', 
			'title' => esc_html__('Background Position', 'payday-loans'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => 'top center', 
			'choices' => array( 
				esc_html__('Top Left', 'payday-loans') . '|top left', 
				esc_html__('Top Center', 'payday-loans') . '|top center', 
				esc_html__('Top Right', 'payday-loans') . '|top right', 
				esc_html__('Center Left', 'payday-loans') . '|center left', 
				esc_html__('Center Center', 'payday-loans') . '|center center', 
				esc_html__('Center Right', 'payday-loans') . '|center right', 
				esc_html__('Bottom Left', 'payday-loans') . '|bottom left', 
				esc_html__('Bottom Center', 'payday-loans') . '|bottom center', 
				esc_html__('Bottom Right', 'payday-loans') . '|bottom right' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'payday-loans' . '_bg_att', 
			'title' => esc_html__('Background Attachment', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'scroll', 
			'choices' => array( 
				esc_html__('Scroll', 'payday-loans') . '|scroll', 
				esc_html__('Fixed', 'payday-loans') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'payday-loans' . '_bg_size', 
			'title' => esc_html__('Background Size', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'cover', 
			'choices' => array( 
				esc_html__('Auto', 'payday-loans') . '|auto', 
				esc_html__('Cover', 'payday-loans') . '|cover', 
				esc_html__('Contain', 'payday-loans') . '|contain' 
			) 
		);
		
		break;
	case 'header':
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_fixed_header', 
			'title' => esc_html__('Fixed Header', 'payday-loans'), 
			'desc' => esc_html__('enable', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_overlaps', 
			'title' => esc_html__('Header Overlaps Content', 'payday-loans'), 
			'desc' => esc_html__('enable', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_top_line', 
			'title' => esc_html__('Top Line', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_top_height', 
			'title' => esc_html__('Top Height', 'payday-loans'), 
			'desc' => esc_html__('pixels', 'payday-loans'), 
			'type' => 'number', 
			'std' => '40', 
			'min' => '30' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_top_line_short_info', 
			'title' => esc_html__('Top Short Info', 'payday-loans'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'payday-loans') . '</strong>', 
			'type' => 'textarea', 
			'std' => '', 
			'class' => '' 
		);
		
	if (CMSMASTERS_DONATIONS) {
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_top_line_donations_but', 
			'title' => esc_html__('Top Donations Button', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_top_line_donations_but_text', 
			'title' => esc_html__('Top Donations Button Text', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => esc_html__('Donate Now!', 'payday-loans'), 
			'class' => 'nohtml' 
		);
	}
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_top_line_add_cont', 
			'title' => esc_html__('Top Additional Content', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'nav', 
			'choices' => array( 
				esc_html__('None', 'payday-loans') . '|none', 
				esc_html__('Top Line Social Icons', 'payday-loans') . '|social', 
				esc_html__('Top Line Navigation', 'payday-loans') . '|nav' 
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_styles', 
			'title' => esc_html__('Header Styles', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'default', 
			'choices' => array( 
				esc_html__('Default Style', 'payday-loans') . '|default', 
				esc_html__('Compact Style Left Navigation', 'payday-loans') . '|l_nav', 
				esc_html__('Compact Style Right Navigation', 'payday-loans') . '|r_nav', 
				esc_html__('Compact Style Center Navigation', 'payday-loans') . '|c_nav'
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_mid_height', 
			'title' => esc_html__('Header Middle Height', 'payday-loans'), 
			'desc' => esc_html__('pixels', 'payday-loans'), 
			'type' => 'number', 
			'std' => '102', 
			'min' => '80' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_bot_height', 
			'title' => esc_html__('Header Bottom Height', 'payday-loans'), 
			'desc' => esc_html__('pixels', 'payday-loans'), 
			'type' => 'number', 
			'std' => '60', 
			'min' => '40' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_search', 
			'title' => esc_html__('Header Search', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
	if (CMSMASTERS_DONATIONS) {
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_donations_but', 
			'title' => esc_html__('Header Donations Button', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_donations_but_text', 
			'title' => esc_html__('Header Donations Button Text', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => esc_html__('Donate Now!', 'payday-loans'), 
			'class' => 'nohtml' 
		);
	}
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_add_cont', 
			'title' => esc_html__('Header Additional Content', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'social', 
			'choices' => array( 
				esc_html__('None', 'payday-loans') . '|none', 
				esc_html__('Header Social Icons', 'payday-loans') . '|social', 
				esc_html__('Header Custom HTML', 'payday-loans') . '|cust_html' 
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'payday-loans' . '_header_add_cont_cust_html', 
			'title' => esc_html__('Header Custom HTML', 'payday-loans'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'payday-loans') . '</strong>', 
			'type' => 'textarea', 
			'std' => '', 
			'class' => '' 
		);
		
		break;
	case 'content':
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_layout', 
			'title' => esc_html__('Layout Type by Default', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio_img', 
			'std' => 'fullwidth', 
			'choices' => array( 
				esc_html__('Right Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_archives_layout', 
			'title' => esc_html__('Archives Layout Type', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio_img', 
			'std' => 'fullwidth', 
			'choices' => array( 
				esc_html__('Right Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_search_layout', 
			'title' => esc_html__('Search Layout Type', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio_img', 
			'std' => 'fullwidth', 
			'choices' => array( 
				esc_html__('Right Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
	if (CMSMASTERS_EVENTS_CALENDAR) {
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_events_layout', 
			'title' => esc_html__('Events Calendar Layout Type', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio_img', 
			'std' => 'fullwidth', 
			'choices' => array( 
				esc_html__('Right Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
	}
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_other_layout', 
			'title' => esc_html__('Other Layout Type', 'payday-loans'), 
			'desc' => 'Layout for pages of non-listed types', 
			'type' => 'radio_img', 
			'std' => 'fullwidth', 
			'choices' => array( 
				esc_html__('Right Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'payday-loans') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_heading_alignment', 
			'title' => esc_html__('Heading Alignment by Default', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'left', 
			'choices' => array( 
				esc_html__('Left', 'payday-loans') . '|left', 
				esc_html__('Right', 'payday-loans') . '|right', 
				esc_html__('Center', 'payday-loans') . '|center' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_heading_scheme', 
			'title' => esc_html__('Heading Custom Color Scheme by Default', 'payday-loans'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => 'default', 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_heading_bg_image_enable', 
			'title' => esc_html__('Heading Background Image Visibility by Default', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_heading_bg_image', 
			'title' => esc_html__('Heading Background Image by Default', 'payday-loans'), 
			'desc' => esc_html__('Choose your custom heading background image by default.', 'payday-loans'), 
			'type' => 'upload', 
			'std' => '', 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_heading_bg_repeat', 
			'title' => esc_html__('Heading Background Repeat by Default', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'no-repeat', 
			'choices' => array( 
				esc_html__('No Repeat', 'payday-loans') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'payday-loans') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'payday-loans') . '|repeat-y', 
				esc_html__('Repeat', 'payday-loans') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_heading_bg_attachment', 
			'title' => esc_html__('Heading Background Attachment by Default', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'scroll', 
			'choices' => array( 
				esc_html__('Scroll', 'payday-loans') . '|scroll', 
				esc_html__('Fixed', 'payday-loans') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_heading_bg_size', 
			'title' => esc_html__('Heading Background Size by Default', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'cover', 
			'choices' => array( 
				esc_html__('Auto', 'payday-loans') . '|auto', 
				esc_html__('Cover', 'payday-loans') . '|cover', 
				esc_html__('Contain', 'payday-loans') . '|contain' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_heading_bg_color', 
			'title' => esc_html__('Heading Background Color Overlay by Default', 'payday-loans'), 
			'desc' => '',  
			'type' => 'rgba', 
			'std' => '' 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_heading_height', 
			'title' => esc_html__('Heading Height by Default', 'payday-loans'), 
			'desc' => esc_html__('pixels', 'payday-loans'), 
			'type' => 'number', 
			'std' => '150', 
			'min' => '0' 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_breadcrumbs', 
			'title' => esc_html__('Breadcrumbs Visibility by Default', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_bottom_scheme', 
			'title' => esc_html__('Bottom Custom Color Scheme', 'payday-loans'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => 'default', 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_bottom_sidebar', 
			'title' => esc_html__('Bottom Sidebar Visibility by Default', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'payday-loans' . '_bottom_sidebar_layout', 
			'title' => esc_html__('Bottom Sidebar Layout by Default', 'payday-loans'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => '14141414', 
			'choices' => array( 
				'1/1|11', 
				'1/2 + 1/2|1212', 
				'1/3 + 2/3|1323', 
				'2/3 + 1/3|2313', 
				'1/4 + 3/4|1434', 
				'3/4 + 1/4|3414', 
				'1/3 + 1/3 + 1/3|131313', 
				'1/2 + 1/4 + 1/4|121414', 
				'1/4 + 1/2 + 1/4|141214', 
				'1/4 + 1/4 + 1/2|141412', 
				'1/4 + 1/4 + 1/4 + 1/4|14141414' 
			) 
		);
		
		break;
	case 'footer':
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_scheme', 
			'title' => esc_html__('Footer Custom Color Scheme', 'payday-loans'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => 'footer', 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_type', 
			'title' => esc_html__('Footer Type', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'small', 
			'choices' => array( 
				esc_html__('Default', 'payday-loans') . '|default', 
				esc_html__('Small', 'payday-loans') . '|small' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_additional_content', 
			'title' => esc_html__('Footer Additional Content', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'social', 
			'choices' => array( 
				esc_html__('None', 'payday-loans') . '|none', 
				esc_html__('Footer Navigation', 'payday-loans') . '|nav', 
				esc_html__('Social Icons', 'payday-loans') . '|social', 
				esc_html__('Custom HTML', 'payday-loans') . '|text' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_logo', 
			'title' => esc_html__('Footer Logo', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_logo_url', 
			'title' => esc_html__('Footer Logo', 'payday-loans'), 
			'desc' => esc_html__('Choose your website footer logo image.', 'payday-loans'), 
			'type' => 'upload', 
			'std' => '|' . get_template_directory_uri() . '/img/logo_footer.png', 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_logo_url_retina', 
			'title' => esc_html__('Footer Logo for Retina', 'payday-loans'), 
			'desc' => esc_html__('Choose your website footer logo image for retina.', 'payday-loans'), 
			'type' => 'upload', 
			'std' => '|' . get_template_directory_uri() . '/img/logo_footer_retina.png', 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_nav', 
			'title' => esc_html__('Footer Navigation', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_social', 
			'title' => esc_html__('Footer Social Icons', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_html', 
			'title' => esc_html__('Footer Custom HTML', 'payday-loans'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'payday-loans') . '</strong>', 
			'type' => 'textarea', 
			'std' => '', 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'payday-loans' . '_footer_copyright', 
			'title' => esc_html__('Copyright Text', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => 'Payday Loans' . ' &copy; ' . date('Y') . ' / ' . esc_html__('All Rights Reserved', 'payday-loans'), 
			'class' => '' 
		);
		
		break;
	}
	
	return $options;	
}

