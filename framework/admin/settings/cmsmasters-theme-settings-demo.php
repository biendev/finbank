<?php 
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version		1.0.0
 * 
 * Admin Panel Theme Settings Import/Export
 * Created by CMSMasters
 * 
 */


function payday_loans_options_demo_tabs() {
	$tabs = array();
	
	
	$tabs['import'] = esc_attr__('Import', 'payday-loans');
	$tabs['export'] = esc_attr__('Export', 'payday-loans');
	
	
	return $tabs;
}


function payday_loans_options_demo_sections() {
	$tab = payday_loans_get_the_tab();
	
	
	switch ($tab) {
	case 'import':
		$sections = array();
		
		$sections['import_section'] = esc_html__('Theme Settings Import', 'payday-loans');
		
		
		break;
	case 'export':
		$sections = array();
		
		$sections['export_section'] = esc_html__('Theme Settings Export', 'payday-loans');
		
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	
	return $sections;
} 


function payday_loans_options_demo_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = payday_loans_get_the_tab();
	}
	
	
	$options = array();
	
	
	switch ($tab) {
	case 'import':
		$options[] = array( 
			'section' => 'import_section', 
			'id' => 'payday-loans' . '_demo_import', 
			'title' => esc_html__('Theme Settings', 'payday-loans'), 
			'desc' => esc_html__("Enter your theme settings data here and click 'Import' button", 'payday-loans'), 
			'type' => 'textarea', 
			'std' => '', 
			'class' => '' 
		);
		
		
		break;
	case 'export':
		$options[] = array( 
			'section' => 'export_section', 
			'id' => 'payday-loans' . '_demo_export', 
			'title' => esc_html__('Theme Settings', 'payday-loans'), 
			'desc' => esc_html__("Click here to export your theme settings data to the file", 'payday-loans'), 
			'type' => 'button', 
			'std' => esc_html__('Export Theme Settings', 'payday-loans'), 
			'class' => 'cmsmasters-demo-export' 
		);
		
		
		break;
	}
	
	
	return $options;	
}

