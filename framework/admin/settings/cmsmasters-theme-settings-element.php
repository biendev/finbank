<?php 
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version 	1.0.0
 * 
 * Admin Panel Element Options
 * Created by CMSMasters
 * 
 */


function payday_loans_options_element_tabs() {
	$tabs = array();
	
	$tabs['sidebar'] = esc_attr__('Sidebars', 'payday-loans');
	$tabs['icon'] = esc_attr__('Social Icons', 'payday-loans');
	$tabs['lightbox'] = esc_attr__('Lightbox', 'payday-loans');
	$tabs['sitemap'] = esc_attr__('Sitemap', 'payday-loans');
	$tabs['error'] = esc_attr__('404', 'payday-loans');
	$tabs['code'] = esc_attr__('Custom Codes', 'payday-loans');
	
	if (class_exists('Cmsmasters_Form_Builder')) {
		$tabs['recaptcha'] = esc_attr__('reCAPTCHA', 'payday-loans');
	}
	
	return $tabs;
}


function payday_loans_options_element_sections() {
	$tab = payday_loans_get_the_tab();
	
	switch ($tab) {
	case 'sidebar':
		$sections = array();
		
		$sections['sidebar_section'] = esc_attr__('Custom Sidebars', 'payday-loans');
		
		break;
	case 'icon':
		$sections = array();
		
		$sections['icon_section'] = esc_attr__('Social Icons', 'payday-loans');
		
		break;
	case 'lightbox':
		$sections = array();
		
		$sections['lightbox_section'] = esc_attr__('Theme Lightbox Options', 'payday-loans');
		
		break;
	case 'sitemap':
		$sections = array();
		
		$sections['sitemap_section'] = esc_attr__('Sitemap Page Options', 'payday-loans');
		
		break;
	case 'error':
		$sections = array();
		
		$sections['error_section'] = esc_attr__('404 Error Page Options', 'payday-loans');
		
		break;
	case 'code':
		$sections = array();
		
		$sections['code_section'] = esc_attr__('Custom Codes', 'payday-loans');
		
		break;
	case 'recaptcha':
		$sections = array();
		
		$sections['recaptcha_section'] = esc_attr__('Form Builder Plugin reCAPTCHA Keys', 'payday-loans');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return $sections;	
} 


function payday_loans_options_element_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = payday_loans_get_the_tab();
	}
	
	$options = array();
	
	switch ($tab) {
	case 'sidebar':
		$options[] = array( 
			'section' => 'sidebar_section', 
			'id' => 'payday-loans' . '_sidebar', 
			'title' => esc_html__('Custom Sidebars', 'payday-loans'), 
			'desc' => '', 
			'type' => 'sidebar', 
			'std' => '' 
		);
		
		break;
	case 'icon':
		$options[] = array( 
			'section' => 'icon_section', 
			'id' => 'payday-loans' . '_social_icons', 
			'title' => esc_html__('Social Icons', 'payday-loans'), 
			'desc' => '', 
			'type' => 'social', 
			'std' => array( 
				'cmsmasters-icon-linkedin|#|' . esc_html__('Linkedin', 'payday-loans') . '|true|#ababab|#c0cf27', 
				'cmsmasters-icon-facebook-1|#|' . esc_html__('Facebook', 'payday-loans') . '|true|#ababab|#c0cf27', 
				'cmsmasters-icon-google|#|' . esc_html__('Google', 'payday-loans') . '|true|#ababab|#c0cf27', 
				'cmsmasters-icon-twitter|#|' . esc_html__('Twitter', 'payday-loans') . '|true|#ababab|#c0cf27', 
				'cmsmasters-icon-youtube-play|#|' . esc_html__('YouTube', 'payday-loans') . '|true|#ababab|#c0cf27' 
			) 
		);
		
		break;
	case 'lightbox':
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_skin', 
			'title' => esc_html__('Skin', 'payday-loans'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => 'dark', 
			'choices' => array( 
				esc_html__('Dark', 'payday-loans') . '|dark', 
				esc_html__('Light', 'payday-loans') . '|light', 
				esc_html__('Mac', 'payday-loans') . '|mac', 
				esc_html__('Metro Black', 'payday-loans') . '|metro-black', 
				esc_html__('Metro White', 'payday-loans') . '|metro-white', 
				esc_html__('Parade', 'payday-loans') . '|parade', 
				esc_html__('Smooth', 'payday-loans') . '|smooth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_path', 
			'title' => esc_html__('Path', 'payday-loans'), 
			'desc' => esc_html__('Sets path for switching windows', 'payday-loans'), 
			'type' => 'radio', 
			'std' => 'vertical', 
			'choices' => array( 
				esc_html__('Vertical', 'payday-loans') . '|vertical', 
				esc_html__('Horizontal', 'payday-loans') . '|horizontal' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_infinite', 
			'title' => esc_html__('Infinite', 'payday-loans'), 
			'desc' => esc_html__('Sets the ability to infinite the group', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_aspect_ratio', 
			'title' => esc_html__('Keep Aspect Ratio', 'payday-loans'), 
			'desc' => esc_html__('Sets the resizing method used to keep aspect ratio within the viewport', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_mobile_optimizer', 
			'title' => esc_html__('Mobile Optimizer', 'payday-loans'), 
			'desc' => esc_html__('Make lightboxes optimized for giving better experience with mobile devices', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_max_scale', 
			'title' => esc_html__('Max Scale', 'payday-loans'), 
			'desc' => esc_html__('Sets the maximum viewport scale of the content', 'payday-loans'), 
			'type' => 'number', 
			'std' => 1, 
			'min' => 0.1, 
			'max' => 2, 
			'step' => 0.05 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_min_scale', 
			'title' => esc_html__('Min Scale', 'payday-loans'), 
			'desc' => esc_html__('Sets the minimum viewport scale of the content', 'payday-loans'), 
			'type' => 'number', 
			'std' => 0.2, 
			'min' => 0.1, 
			'max' => 2, 
			'step' => 0.05 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_inner_toolbar', 
			'title' => esc_html__('Inner Toolbar', 'payday-loans'), 
			'desc' => esc_html__('Bring buttons into windows, or let them be over the overlay', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_smart_recognition', 
			'title' => esc_html__('Smart Recognition', 'payday-loans'), 
			'desc' => esc_html__('Sets content auto recognize from web pages', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_fullscreen_one_slide', 
			'title' => esc_html__('Fullscreen One Slide', 'payday-loans'), 
			'desc' => esc_html__('Decide to fullscreen only one slide or hole gallery the fullscreen mode', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_fullscreen_viewport', 
			'title' => esc_html__('Fullscreen Viewport', 'payday-loans'), 
			'desc' => esc_html__('Sets the resizing method used to fit content within the fullscreen mode', 'payday-loans'), 
			'type' => 'select', 
			'std' => 'center', 
			'choices' => array( 
				esc_html__('Center', 'payday-loans') . '|center', 
				esc_html__('Fit', 'payday-loans') . '|fit', 
				esc_html__('Fill', 'payday-loans') . '|fill', 
				esc_html__('Stretch', 'payday-loans') . '|stretch' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_controls_toolbar', 
			'title' => esc_html__('Toolbar Controls', 'payday-loans'), 
			'desc' => esc_html__('Sets buttons be available or not', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_controls_arrows', 
			'title' => esc_html__('Arrow Controls', 'payday-loans'), 
			'desc' => esc_html__('Enable the arrow buttons', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_controls_fullscreen', 
			'title' => esc_html__('Fullscreen Controls', 'payday-loans'), 
			'desc' => esc_html__('Sets the fullscreen button', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_controls_thumbnail', 
			'title' => esc_html__('Thumbnails Controls', 'payday-loans'), 
			'desc' => esc_html__('Sets the thumbnail navigation', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_controls_keyboard', 
			'title' => esc_html__('Keyboard Controls', 'payday-loans'), 
			'desc' => esc_html__('Sets the keyboard navigation', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_controls_mousewheel', 
			'title' => esc_html__('Mouse Wheel Controls', 'payday-loans'), 
			'desc' => esc_html__('Sets the mousewheel navigation', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_controls_swipe', 
			'title' => esc_html__('Swipe Controls', 'payday-loans'), 
			'desc' => esc_html__('Sets the swipe navigation', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'payday-loans' . '_ilightbox_controls_slideshow', 
			'title' => esc_html__('Slideshow Controls', 'payday-loans'), 
			'desc' => esc_html__('Enable the slideshow feature and button', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		break;
	case 'sitemap':
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'payday-loans' . '_sitemap_nav', 
			'title' => esc_html__('Website Pages', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'payday-loans' . '_sitemap_categs', 
			'title' => esc_html__('Blog Archives by Categories', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'payday-loans' . '_sitemap_tags', 
			'title' => esc_html__('Blog Archives by Tags', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'payday-loans' . '_sitemap_month', 
			'title' => esc_html__('Blog Archives by Month', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'payday-loans' . '_sitemap_pj_categs', 
			'title' => esc_html__('Portfolio Archives by Categories', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'payday-loans' . '_sitemap_pj_tags', 
			'title' => esc_html__('Portfolio Archives by Tags', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		break;
	case 'error':
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_color', 
			'title' => esc_html__('Text Color', 'payday-loans'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => '#cddc39' 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_bg_color', 
			'title' => esc_html__('Background Color', 'payday-loans'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => '#3e467b' 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_bg_img_enable', 
			'title' => esc_html__('Background Image Visibility', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 0 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_bg_image', 
			'title' => esc_html__('Background Image', 'payday-loans'), 
			'desc' => esc_html__('Choose your custom error page background image.', 'payday-loans'), 
			'type' => 'upload', 
			'std' => '', 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_bg_rep', 
			'title' => esc_html__('Background Repeat', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'no-repeat', 
			'choices' => array( 
				esc_html__('No Repeat', 'payday-loans') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'payday-loans') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'payday-loans') . '|repeat-y', 
				esc_html__('Repeat', 'payday-loans') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_bg_pos', 
			'title' => esc_html__('Background Position', 'payday-loans'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => 'top center', 
			'choices' => array( 
				esc_html__('Top Left', 'payday-loans') . '|top left', 
				esc_html__('Top Center', 'payday-loans') . '|top center', 
				esc_html__('Top Right', 'payday-loans') . '|top right', 
				esc_html__('Center Left', 'payday-loans') . '|center left', 
				esc_html__('Center Center', 'payday-loans') . '|center center', 
				esc_html__('Center Right', 'payday-loans') . '|center right', 
				esc_html__('Bottom Left', 'payday-loans') . '|bottom left', 
				esc_html__('Bottom Center', 'payday-loans') . '|bottom center', 
				esc_html__('Bottom Right', 'payday-loans') . '|bottom right' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_bg_att', 
			'title' => esc_html__('Background Attachment', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'scroll', 
			'choices' => array( 
				esc_html__('Scroll', 'payday-loans') . '|scroll', 
				esc_html__('Fixed', 'payday-loans') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_bg_size', 
			'title' => esc_html__('Background Size', 'payday-loans'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'cover', 
			'choices' => array( 
				esc_html__('Auto', 'payday-loans') . '|auto', 
				esc_html__('Cover', 'payday-loans') . '|cover', 
				esc_html__('Contain', 'payday-loans') . '|contain' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_search', 
			'title' => esc_html__('Search Line', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_sitemap_button', 
			'title' => esc_html__('Sitemap Button', 'payday-loans'), 
			'desc' => esc_html__('show', 'payday-loans'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'payday-loans' . '_error_sitemap_link', 
			'title' => esc_html__('Sitemap Page URL', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => '', 
			'class' => '' 
		);
		
		break;
	case 'code':
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'payday-loans' . '_custom_css', 
			'title' => esc_html__('Custom CSS', 'payday-loans'), 
			'desc' => '', 
			'type' => 'textarea', 
			'std' => '', 
			'class' => 'allowlinebreaks' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'payday-loans' . '_custom_js', 
			'title' => esc_html__('Custom JavaScript', 'payday-loans'), 
			'desc' => '', 
			'type' => 'textarea', 
			'std' => '', 
			'class' => 'allowlinebreaks' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'payday-loans' . '_gmap_api_key', 
			'title' => esc_html__('Google Maps API key', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => '', 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'payday-loans' . '_api_key', 
			'title' => esc_html__('Twitter API key', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => '', 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'payday-loans' . '_api_secret', 
			'title' => esc_html__('Twitter API secret', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => '', 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'payday-loans' . '_access_token', 
			'title' => esc_html__('Twitter Access token', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => '', 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'payday-loans' . '_access_token_secret', 
			'title' => esc_html__('Twitter Access token secret', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => '', 
			'class' => '' 
		);
		
		break;
	case 'recaptcha':
		$options[] = array( 
			'section' => 'recaptcha_section', 
			'id' => 'payday-loans' . '_recaptcha_public_key', 
			'title' => esc_html__('reCAPTCHA Public Key', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => '', 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'recaptcha_section', 
			'id' => 'payday-loans' . '_recaptcha_private_key', 
			'title' => esc_html__('reCAPTCHA Private Key', 'payday-loans'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => '', 
			'class' => '' 
		);
		
		break;
	}
	
	return $options;	
}

