/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version		1.0.3
 * 
 * Visual Content Composer Schortcodes Extend
 * Created by CMSMasters
 * 
 */
 




/**
 * Heading Extend
 */

var cmsmasters_heading_new_fields = {};


for (var id in cmsmastersShortcodes.cmsmasters_heading.fields) {
	if (id === 'animation') {
		cmsmasters_heading_new_fields['custom_check'] = { 
			type : 		'checkbox', 
			title : 	composer_shortcodes_extend.heading_field_custom_check, 
			descr : 	'', 
			def : 		'', 
			required : 	false, 
			width : 	'half',  
			choises : { 
				'true' : cmsmasters_shortcodes.choice_enable 
			}
		};
		cmsmasters_heading_new_fields['width_monitor'] = { 
			type : 		'input', 
			title : 	composer_shortcodes_extend.heading_field_width_monitor, 
			descr : 	"<span>" + cmsmasters_shortcodes.note + ' ' + composer_shortcodes_extend.heading_field_size_zero_note + "</span>", 
			def : 		'767', 
			required : 	false, 
			width : 	'number', 
			min : 		'320', 
			depend : 	'custom_check:true' 
		};
		cmsmasters_heading_new_fields['custom_font_size'] = { 
			type : 		'input', 
			title : 	composer_shortcodes_extend.heading_field_custom_font_size, 
			descr : 	"<span>" + cmsmasters_shortcodes.note + ' ' + composer_shortcodes_extend.heading_field_size_zero_note + "</span>", 
			def : 		'', 
			required : 	false, 
			width : 	'number', 
			min : 		'0', 
			depend : 	'custom_check:true' 
		};
		cmsmasters_heading_new_fields['custom_line_height'] = { 
			type : 		'input', 
			title : 	composer_shortcodes_extend.heading_field_custom_line_height, 
			descr : 	"<span>" + cmsmasters_shortcodes.note + ' ' + composer_shortcodes_extend.heading_field_size_zero_note + "</span>", 
			def : 		'', 
			required : 	false, 
			width : 	'number', 
			min : 		'0', 
			depend : 	'custom_check:true' 
		};
		
		cmsmasters_heading_new_fields[id] = cmsmastersShortcodes.cmsmasters_heading.fields[id];
	} else {
		cmsmasters_heading_new_fields[id] = cmsmastersShortcodes.cmsmasters_heading.fields[id];
	}
}


cmsmastersShortcodes.cmsmasters_heading.fields = cmsmasters_heading_new_fields;


/**
 * Posts Slider Extend
 */

var posts_slider_new_fields = {};


for (var id in cmsmastersShortcodes.cmsmasters_posts_slider.fields) {
	
	
	if (id === 'columns') {
		
		posts_slider_new_fields['controls'] = { 
			type : 		'checkbox', 
			title : 	composer_shortcodes_extend.posts_slider_controls_enable_title, 
			descr : 	'', 
			def : 		'', 
			required : 	false, 
			width : 	'half', 
			choises : { 
						'true' : 	cmsmasters_shortcodes.choice_enable 
			}
		};
		
		posts_slider_new_fields[id] = cmsmastersShortcodes.cmsmasters_posts_slider.fields[id];
	} else {
		posts_slider_new_fields[id] = cmsmastersShortcodes.cmsmasters_posts_slider.fields[id];
	}
}


cmsmastersShortcodes.cmsmasters_posts_slider.fields = posts_slider_new_fields;


var cmsmasters_posts_slider_new_fields = {};


for (var id in cmsmastersShortcodes.cmsmasters_posts_slider.fields) {
	if (id === 'amount') { // Delete Field
		delete cmsmastersShortcodes.cmsmasters_posts_slider.fields[id];
	} else if (id === 'columns') { // Delete Field Attribute
		delete cmsmastersShortcodes.cmsmasters_posts_slider.fields[id]['depend'];  
		
		
		cmsmasters_posts_slider_new_fields[id] = cmsmastersShortcodes.cmsmasters_posts_slider.fields[id];
	} else {
		cmsmasters_posts_slider_new_fields[id] = cmsmastersShortcodes.cmsmasters_posts_slider.fields[id];
	}
}


cmsmastersShortcodes.cmsmasters_posts_slider.fields = cmsmasters_posts_slider_new_fields;



/**
 * Portfolio Extend
 */
 
var cmsmasters_portfolio_new_fields = {};

for (var id in cmsmastersShortcodes.cmsmasters_portfolio.fields) {
	if (id === 'filter_text') {
		delete cmsmastersShortcodes.cmsmasters_portfolio.fields[id];
	} else {
		cmsmasters_portfolio_new_fields[id] = cmsmastersShortcodes.cmsmasters_portfolio.fields[id];
	}
}


cmsmastersShortcodes.cmsmasters_portfolio.fields = cmsmasters_portfolio_new_fields;


var cmsmasters_portfolio_new_fields = {};


for (var id in cmsmastersShortcodes.cmsmasters_portfolio.fields) {

	if (id === 'metadata_puzzle') {
		cmsmastersShortcodes.cmsmasters_portfolio.fields[id]['choises'] = {
			'title' : 		cmsmasters_shortcodes.choice_title, 
			'categories' : 	cmsmasters_shortcodes.choice_categories, 
			'comments' : 	cmsmasters_shortcodes.choice_comments, 
			'likes' : 		cmsmasters_shortcodes.choice_likes
		};
		
		
		cmsmasters_portfolio_new_fields[id] = cmsmastersShortcodes.cmsmasters_portfolio.fields[id];
	} else {
		cmsmasters_portfolio_new_fields[id] = cmsmastersShortcodes.cmsmasters_portfolio.fields[id];
	}
}


cmsmastersShortcodes.cmsmasters_portfolio.fields = cmsmasters_portfolio_new_fields;



/**
 * Featured Block Extend
 */

var featured_block_new_fields = {};


for (var id in cmsmastersShortcodes.cmsmasters_featured_block.fields) {
	
	if (id === 'animation') {
		
		featured_block_new_fields['bd_color'] = { 
			type : 		'rgba', 
			title : 	composer_shortcodes_extend.cmsmasters_featured_block_field_bd_color, 
			descr : 	'', 
			def : 		'', 
			required : 	false, 
			width : 	'half' 
		};
		
		featured_block_new_fields['bd_width'] = { 
			type : 		'range', 
			title : 	composer_shortcodes_extend.cmsmasters_featured_block_field_bd_width, 
			descr : 	"<span>" + cmsmasters_shortcodes.note + ' ' + cmsmasters_shortcodes.size_note_pixel + "</span>", 
			def : 		'0', 
			required : 	true, 
			width : 	'number', 
			min : 		'0', 
			max : 		'20' 
		};
		
		featured_block_new_fields[id] = cmsmastersShortcodes.cmsmasters_featured_block.fields[id];
	} else {
		featured_block_new_fields[id] = cmsmastersShortcodes.cmsmasters_featured_block.fields[id];
	}
}


cmsmastersShortcodes.cmsmasters_featured_block.fields = featured_block_new_fields;
