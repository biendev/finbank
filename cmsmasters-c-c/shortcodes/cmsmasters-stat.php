<?php
/**
 * @package 	WordPress
 * @subpackage 	Payday Loans
 * @version 	1.0.8
 * 
 * Content Composer Single Progress Bar Shortcode
 * Created by CMSMasters
 * 
 */
 
 
extract(shortcode_atts($new_atts, $atts));


$unique_id = uniqid();


$this->stats_atts['style_stats'] .= "\n" . '.cmsmasters_stats.shortcode_animated #cmsmasters_stat_' . $unique_id . '.cmsmasters_stat { ' . 
	"\n\t" . (($this->stats_atts['stats_type'] == 'horizontal') ? 'width' : 'height') . ':' . $progress . '%; ' . 
"\n" . '} ' . "\n\n" .
'#cmsmasters_stat_' . $unique_id . ' .cmsmasters_stat_inner .cmsmasters_stat_title:before { ' . 
	(($color != '') ? "\n\t" . cmsmasters_color_css('color', $color) : '') . 
"\n" . '} ' . "\n\n" .
'.stats_mode_circles #cmsmasters_stat_' . $unique_id . ' .cmsmasters_stat_inner:before, ' . 
'.stats_mode_circles #cmsmasters_stat_' . $unique_id . ' .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap { ' . 
	(($color != '') ? "\n\t" . cmsmasters_color_css('color', $color) : '') . 
"\n" . '} ' . "\n\n" .
'.stats_mode_bars #cmsmasters_stat_' . $unique_id . ' .cmsmasters_stat_inner { ' . 
	(($color != '') ? "\n\t" . cmsmasters_color_css('background-color', $color) : '') . 
"\n" . '} ' . "\n";


$out = '<div class="cmsmasters_stat_wrap' . (($this->stats_atts['stats_mode'] == 'circles' || ($this->stats_atts['stats_mode'] == 'bars' && $this->stats_atts['stats_type'] == 'vertical')) ? $this->stats_atts['stats_count'] : '') . '" '. (($this->stats_atts['stats_mode'] == 'bars') ? ' data-percent="' . $progress . '"' : '') . ' >' . "\n" . 
	(($this->stats_atts['stats_mode'] == 'bars' && $this->stats_atts['stats_type'] == 'vertical') ? '<div class="cmsmasters_stat_container">' . "\n" : '') . 
		(($this->stats_atts['stats_mode'] == 'bars' && $this->stats_atts['stats_type'] == 'horizontal') ?
			'<span class="cmsmasters_stat_counter_wrap">' . "\n" . 
				'<span class="cmsmasters_stat_counter">' . (($this->stats_atts['stats_mode'] == 'bars') ? $progress : '0') . '</span>' . 
				'<span class="cmsmasters_stat_units">%</span>' . "\n" . 
			'</span>' . "\n" : '') . 
		'<div id="cmsmasters_stat_' . $unique_id . '" class="cmsmasters_stat' . 
		(($classes != '') ? ' ' . $classes : '') . 
		(($content == '' && $icon == '') ? ' stat_only_number' : '') . 
		(($content != '' && $icon != '') ? ' stat_has_titleicon' : '') . '"' . 
		(($this->stats_atts['stats_mode'] == 'circles') ? ' data-percent="' . $progress . '"' : '') . 
		'>' . "\n" . 
			'<div class="cmsmasters_stat_inner' . 
			(($this->stats_atts['stats_mode'] == 'circles' && $icon != '' || $this->stats_atts['stats_type'] == 'vertical' && $icon != '') ? ' ' . $icon : '') . 
			(($this->stats_atts['stats_mode'] == 'bars' && $this->stats_atts['stats_type'] == 'vertical' && $color != '') ? ' cmsmasters_bars_custom_color' : '') . 
			'">' . "\n" . 
				(($content != '' && $this->stats_atts['stats_mode'] == 'bars' && $this->stats_atts['stats_type'] == 'horizontal') ? '<span class="cmsmasters_stat_title' . 
			(($this->stats_atts['stats_mode'] == 'bars' && $icon != '') ? ' ' . $icon : '') . 
			'">' . $content . '</span>' . "\n" : '') . 
				(($this->stats_atts['stats_mode'] == 'circles' || $this->stats_atts['stats_mode'] == 'bars' && $this->stats_atts['stats_type'] == 'vertical') ?
					'<span class="cmsmasters_stat_counter_wrap">' . "\n" . 
						'<span class="cmsmasters_stat_counter">' . (($this->stats_atts['stats_mode'] == 'bars') ? $progress : '0') . '</span>' . 
						'<span class="cmsmasters_stat_units">%</span>' . "\n" . 
					'</span>' . "\n" : '') . 
			'</div>' . "\n" . 
		'</div>' . "\n" . 
	(($this->stats_atts['stats_mode'] == 'bars' && $this->stats_atts['stats_type'] == 'vertical') ? '</div>' . "\n" : '') . 
	(($content != '' && $this->stats_atts['stats_mode'] == 'bars' && $this->stats_atts['stats_type'] == 'vertical') ? '<span class="cmsmasters_stat_title">' . $content . '</span>' . "\n" : '') . 
	(($content != '' && $this->stats_atts['stats_mode'] == 'circles') ? '<span class="cmsmasters_stat_title">' . $content . '</span>' . "\n" : '') . 
	(($subtitle != '') ? '<span class="cmsmasters_stat_subtitle">' . $subtitle . '</span>' . "\n" : '') . 
'</div>';


echo payday_loans_return_content($out);